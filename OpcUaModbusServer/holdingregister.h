#pragma once
#include "modbusreadcoil.h" 

class HoldingRegister
{
public:
	HoldingRegister();
	virtual ~HoldingRegister();
	void connect(boost::asio::ip::tcp::endpoint & ep);
	void connectHandler(const boost::system::error_code & ec);
	void writeFrame();
	void writeFrameHanlder(const boost::system::error_code& e, size_t byte);
	void read();
	void readHandler(const boost::system::error_code& e, size_t byte);
	std::vector<int> repVector;
private:
	uint8_t respone[1024];
	std::vector<int> tempVector;
	//short addr_, num_value_;
	std::thread threadWork;
	boost::asio::io_service service;
	boost::asio::io_service::work work;
	boost::asio::ip::tcp::socket sock;
	uint8_t frame[12];

	void timeout(const boost::system::error_code&e);
	void wait();
	boost::asio::deadline_timer t;
};