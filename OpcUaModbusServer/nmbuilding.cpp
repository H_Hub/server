﻿#include "tagmongodb.h"
#include "nmbuilding.h"
#include "tagmodbusobject.h"
#include "modbusobject.h"


class TagModbusObject;

class BaseVariableType : public OpcUa::BaseDataVariableType
{
public: 
	BaseVariableType(
		const UaNodeId&    nodeId,
		const UaString&    name,
		OpcUa_UInt16       browseNameNameSpaceIndex,
		const UaVariant&   initialValue,
		OpcUa_Byte         accessLevel,
		NodeManagerConfig* pNodeConfig,
		UaMutexRefCounted* pSharedMutex = NULL)
		: OpcUa::BaseDataVariableType(nodeId, name, browseNameNameSpaceIndex, initialValue, accessLevel, pNodeConfig, pSharedMutex), m_address(0)
	{}
	virtual ~BaseVariableType() {}
	void setAddress(int address) {
		m_address = address;
	}
	int getAddress() const {
		return m_address;
	}
	virtual int  getVariableType() = 0; // pure virtual	

private:
	int m_address;
};

class CoilVariableType : public BaseVariableType
{
public:
	CoilVariableType(
		const UaNodeId&    nodeId,
		const UaString&    name,
		OpcUa_UInt16       browseNameNameSpaceIndex,
		const UaVariant&   initialValue,
		OpcUa_Byte         accessLevel,
		NodeManagerConfig* pNodeConfig,
		UaMutexRefCounted* pSharedMutex = NULL)
		: BaseVariableType(nodeId, name, browseNameNameSpaceIndex, initialValue, accessLevel, pNodeConfig, pSharedMutex), m_address(0)
	{}
	
	virtual ~CoilVariableType() {}
	/*void setAddress(int address) {
		m_address = address;
	}

	int getAddress() const {
		return m_address;
	}*/
	UaNodeId typeDefinitionId() const override
	{
		UaNodeId id(10001, 2);		
		return id;
	};
	virtual int  getVariableType() override
	{
		return 1 ; //1 = CoilVariableType
	};

private:
	int m_address; 
};

class RegVariableType : public BaseVariableType
{
public:
	RegVariableType(
		const UaNodeId&    nodeId,
		const UaString&    name,
		OpcUa_UInt16       browseNameNameSpaceIndex,
		const UaVariant&   initialValue,
		OpcUa_Byte         accessLevel,
		NodeManagerConfig* pNodeConfig,
		UaMutexRefCounted* pSharedMutex = NULL)
		: BaseVariableType(nodeId, name, browseNameNameSpaceIndex, initialValue, accessLevel, pNodeConfig, pSharedMutex), m_address(0)
	{}	
	virtual ~RegVariableType() {}
	/*void setAddress(int address) {
		m_address = address;
	}

	int getAddress() const {
		return m_address;
	}*/
	virtual int getVariableType() override
	{
		return 2; // 2 = RegVariableType
	}
	UaNodeId typeDefinitionId() const override
	{
		UaNodeId id(10002, 2);
		return id;
	};

private:
	int m_address;

};

NmBuilding::NmBuilding() : NodeManagerBase("urn:HVH:OpcUaModbusSerer:BuildingOpcUaModbusServer")
{
	
}

NmBuilding::~NmBuilding()
{
}

UaStatus NmBuilding::afterStartUp()
{
	UaStatus ret;
	TagModbusObject * pTagModbus =NULL;
	UaString sName;

	OpcUa_Int32 i;
	UaString sModbusName;
	OpcUa_UInt32 count = 1 ;
	NmBuilding* pNodeManager;
	OpcUa_UInt32 modbusAddress;	

	createTypeNode();
	
	///-Create folder "ModbusOpcUa" and add the folder to the ObjectFolder
	pFolder = new UaFolder("ModbusOpcUa", UaNodeId("ModbusOpcUa", getNameSpaceIndex()), m_defaultLocaleId);
	ret = addNodeAndReference(OpcUaId_ObjectsFolder, pFolder, OpcUaId_HasChild);

	
	for (i = 0; i < count; i++)
	{
		ret = getModbusConfig(i, sModbusName, modbusAddress); // "getModbusConfig" be used to get name for sModbusName ("TagModbusObject")
		pTagModbus = new TagModbusObject(
			sModbusName,
			UaNodeId(sModbusName, getNameSpaceIndex()),
			m_defaultLocaleId,
			this,
			modbusAddress);
		ret = addNodeAndReference(pFolder, pTagModbus, OpcUaId_HasComponent);
		UA_ASSERT(ret.isGood());
	}

	mongocxx::uri uri("mongodb://127.0.0.1:27017");
	mongocxx::client client(uri);
	mongocxx::database db = client["tagModbusDB"];
	mongocxx::collection col = db["ModbusCoil"];	

	//Query mongodb, document...
	auto cursor = db["ModbusCoil"].find({}); //find all
	for (auto&& doc : cursor)
	{
		bsoncxx::document::element element = doc["coil"];
		if (element && element.type() == bsoncxx::type::k_int32)
		{
			coil.push_back(element.get_int32().value); 
		}
	}	
			//Add tag//Add node from mongodb
	for (auto j : coil)
	{
		CoilVariableType* pDataVariable;
		//OpcUa::DataItemType* pDataItem;
		UaVariant defaultValue;
		UaStatus addStatus;
		//Add Variable "AddedNode" as BaseDataVariable
		defaultValue.setString(pModbusMaster->strVector[j].c_str());
		UaString sDisplayName = UaString("TagModbus_Coil%1").arg(j);
		UaString sNodeId = UaString("coil%1").arg(j);
		pDataVariable = new CoilVariableType(
			UaNodeId(sNodeId, getNameSpaceIndex()),
			sDisplayName,
			getNameSpaceIndex(),
			defaultValue,
			Ua_AccessLevel_CurrentRead | Ua_AccessLevel_CurrentWrite,
			this);//Node manager for this variable (pDataVariableTag)
		pDataVariable->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
		pDataVariable->setValueHandling(UaVariable_Value_CacheIsSource);
		addStatus = addNodeAndReference(pFolder, pDataVariable, OpcUaId_HasComponent);
		UA_ASSERT(addStatus.isGood());

		
		pDataVariable->setAddress(j);

	}
	mongocxx::collection colRegister = db["ModbusRegister"];
	auto cursorReg = colRegister.find({});
	for (auto&& doc:cursorReg)
	{
		bsoncxx::document::element ele = doc["Register"]; 
		if (ele && ele.type() == bsoncxx::type::k_int32)
		{
			reg.push_back(ele.get_int32().value);
		}
	}
	for (auto k : reg)
	{
		RegVariableType* pDataVariable;
		UaVariant value;
		value.setInt32(pHoldingRegister->repVector[k]);
		UaStatus addStatus;
		UaString sDisplayName = UaString("TagModbus_Register%1").arg(k);
		UaString sNodeId = UaString("reg%1").arg(k);
		pDataVariable = new RegVariableType(
			UaNodeId(sNodeId, getNameSpaceIndex()),
			sDisplayName,
			getNameSpaceIndex(),
			value,
			Ua_AccessLevel_CurrentRead | Ua_AccessLevel_CurrentWrite,
			this);//Node manager for this variable (pDataVariableTag)
		pDataVariable->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
		pDataVariable->setValueHandling(UaVariable_Value_CacheIsSource);
		addStatus = addNodeAndReference(pFolder, pDataVariable, OpcUaId_HasComponent);
		UA_ASSERT(addStatus.isGood());
		pDataVariable->setAddress(k);
	}
	return ret;
}

UaStatus NmBuilding::beforeShutDown()
{
	UaStatus ret;
	return ret;
}


UaStatus NmBuilding::readValues()
{
	UaStatus ret;
	UaVariable* pInstanceDeclaration = NULL;
	/// handling datachange here
	//Varuiabale "coil" of TagModbusObject
	
	pInstanceDeclaration = getInstanceDeclarationVariable("point");
	UaVariant uaCoil0;
	uaCoil0.setString(pModbusMaster->strVector[0].c_str());
	UaDataValue dt0(uaCoil0, OpcUa_Good, UaDateTime::now(), UaDateTime::now());
	pInstanceDeclaration->setValue(NULL, dt0, OpcUa_False);

	//Variable "statuscoil" of TagModbusObject	
	pInstanceDeclaration = getInstanceDeclarationVariable("statuscoil");	
	UaVariant uaCoil1;
	uaCoil1.setString(pModbusMaster->strVector[1].c_str());
	UaDataValue dt1(uaCoil1, OpcUa_Good, UaDateTime::now(), UaDateTime::now());
	pInstanceDeclaration->setValue(NULL, dt1, OpcUa_False); 
	
	for (auto c : coil)
	{
		UaString sCoilId = UaString("coil%1").arg(c);
		pInstanceDeclaration = getInstanceDeclarationVariable(sCoilId);
		UaVariant uaCoil;
		uaCoil.setString(pModbusMaster->strVector[c].c_str());
		UaDataValue dt2(uaCoil, OpcUa_Good, UaDateTime::now(), UaDateTime::now());
		pInstanceDeclaration->setValue(NULL, dt2, OpcUa_False);
	}
	for (auto r : reg)
	{
		UaString sRegId = UaString("reg%1").arg(r);
		pInstanceDeclaration = getInstanceDeclarationVariable(sRegId);
		UaVariant uaReg;
		uaReg.setInt32( pHoldingRegister->repVector[r]); // phai dung method .setInt, ko the gan = dc.
		///uaReg.setString(pModbusMaster->strVector[c].c_str());
		UaDataValue dt3(uaReg, OpcUa_Good, UaDateTime::now(), UaDateTime::now());
		pInstanceDeclaration->setValue(NULL, dt3, OpcUa_False);
	}
		
	
	
	return ret;
}


OpcUa_Boolean NmBuilding::beforeSetAttributeValue(Session * pSession, UaNode * pNode, OpcUa_Int32 attributeId, const UaDataValue & dataValue, OpcUa_Boolean & checkWriteMask)
{
	
	OpcUa_UInt32 typeDefinitionId = pNode->typeDefinitionId().identifierNumeric();

	BaseVariableType* pBaseVaribleType= static_cast<BaseVariableType*> (pNode);
	int type = pBaseVaribleType->getVariableType();
	
	if (type == 1)
	{
		CoilVariableType* pCoilVariableType = static_cast<CoilVariableType*> (pNode);
		int k = pCoilVariableType->getAddress();
		pModbusWriteCoil->writtenAddr_ = (short)k;
		UaString value = dataValue.value()->Value.String;
		UaVariant tempValue = const_cast<OpcUa_Variant*>(dataValue.value());
		boost::asio::ip::tcp::endpoint ep(boost::asio::ip::address_v4::from_string("127.0.0.1"), 502); //ModbusSlave
		if (value == "0") {
			pModbusWriteCoil->valueWrite_ = 0;
		}
		if (value == "1") {
			pModbusWriteCoil->valueWrite_ = 255;
		}
		pModbusWriteCoil->connect(ep);
	}
	if (type ==2)
	{
		RegVariableType* pRegVariableType = static_cast<RegVariableType*>(pNode);
		int k = pRegVariableType->getAddress(); 
		pWriteRegister->addr = (short)k;
		int v = dataValue.value()->Value.Int32;
		boost::asio::ip::tcp::endpoint ep503(boost::asio::ip::address_v4::from_string("127.0.0.1"), 503);//ModbusSlave device
		pWriteRegister->num_val = v;
		pWriteRegister->connect(ep503);
	}
	
	return OpcUa_True;
}



UaVariable * NmBuilding::getInstanceDeclarationVariable(UaString stringIdentifier)
{
	
	UaNode* pNode = findNode(UaNodeId(stringIdentifier, getNameSpaceIndex()));
	if ((pNode != NULL) && (pNode->nodeClass() == OpcUa_NodeClass_Variable))
	{
		return (UaVariable*)pNode;
	}
	else
	{
		return NULL;
	}
}


UaStatus NmBuilding::createTypeNode()
{
	UaStatus ret;
	UaStatus addStatus;

	UaObjectTypeSimple* pOpcModbusType =NULL;
	UaObjectTypeSimple* pTagModbusType = NULL;
	UaVariant defaultValue;
	
	OpcUa::BaseDataVariableType* pDataVariable;
	OpcUa::DataItemType* pDataItem;
	std::string test = pModbusMaster->strVector[0];
	UaString p1 = test.c_str();

	// CREATE THE OPCMODBUS TYPE
	//Add object OPCModbusType
	pOpcModbusType = new UaObjectTypeSimple(
		"OpcModbusType",
		UaNodeId("opcmodbustype", getNameSpaceIndex()),
		m_defaultLocaleId,
		OpcUa_True);	//lop truu tuong (abstract)? -> ko the khoi tao
	addStatus = addNodeAndReference(OpcUaId_BaseObjectType, pOpcModbusType, OpcUaId_HasSubtype);
	UA_ASSERT(addStatus.isGood());

	/// CREATE THE OPCMODBUS TYPE Instance Declaration
	//Add Variable "Tag" as BaseDataVariable
	defaultValue.setString(p1);

	pDataVariable = new OpcUa::BaseDataVariableType(
		UaNodeId("tag", getNameSpaceIndex()),
		"Tag",
		getNameSpaceIndex(),
		defaultValue,
		Ua_AccessLevel_CurrentRead | Ua_AccessLevel_CurrentWrite,
		this);//Node manager for this variable (pDataVariableTag)
	pDataVariable->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
	pDataVariable->setValueHandling(UaVariable_Value_CacheIsSource);
	addStatus = addNodeAndReference(pOpcModbusType, pDataVariable, OpcUaId_HasComponent);
	UA_ASSERT(addStatus.isGood());

	//CREATE THE TAGMODBUS TYPE

	pTagModbusType = new UaObjectTypeSimple(
		"TagModbusType",
		UaNodeId("tagmodbustype",getNameSpaceIndex()),
		m_defaultLocaleId,
		OpcUa_False	);
	addStatus = addNodeAndReference(pOpcModbusType, pTagModbusType, OpcUaId_HasSubtype);
	UA_ASSERT(addStatus.isGood());
	///create instance declaration of TagModbus type
	
	///ADD POINT HERE
	//Add variable "Point" 
	std::string sCoil0 = pModbusMaster->strVector[0];
	UaString uaCoil0 = sCoil0.c_str();
	defaultValue.setString(uaCoil0);
	pDataItem = new  OpcUa::DataItemType (
		UaNodeId("point", getNameSpaceIndex()),
		"Point",
		getNameSpaceIndex(),
		defaultValue,
		(Ua_AccessLevel_CurrentRead |Ua_AccessLevel_CurrentWrite),
		this);//Node manager for this variable (pDataVariableTag)
	pDataItem->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
	addStatus = addNodeAndReference(pTagModbusType, pDataItem, OpcUaId_HasComponent);
	UA_ASSERT(addStatus.isGood());
	/*UaVariant value;
	value.setString(uaCoil0);
	UaDataValue dataValue(value, OpcUa_Good, UaDateTime::now(), UaDateTime::now());*/
	pDataItem->setValueHandling(UaVariable_Value_CacheIsSource);
	
	//pDataItem->setValue(nullptr, dataValue, OpcUa_False);	

	//add variable "Voltage"
	defaultValue.setInt32(10);
	pDataItem = new  OpcUa::DataItemType(
		UaNodeId("voltage", getNameSpaceIndex()),
		"Voltage",
		getNameSpaceIndex(),
		defaultValue,
		(Ua_AccessLevel_CurrentWrite  | Ua_AccessLevel_CurrentRead),
		this);//Node manager for this variable (pDataVariableTag)
	pDataItem->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
	pDataItem->setValueHandling(UaVariable_Value_CacheIsSource); //chinh la kieu mac dinh cua OPCUA la UaVriable_Value_CacheIsSource
	addStatus = addNodeAndReference(pTagModbusType, pDataItem, OpcUaId_HasComponent);
	UA_ASSERT(addStatus.isGood());

	//add variable "statuscoil"
	std::string sCoil1 = pModbusMaster->strVector[1];
	UaString uaCoil1 = sCoil1.c_str();
	defaultValue.setString(uaCoil1);
	pDataItem = new  OpcUa::DataItemType(
		UaNodeId("statuscoil", getNameSpaceIndex()),
		"StatusCoil",
		getNameSpaceIndex(),
		defaultValue,
		Ua_AccessLevel_CurrentRead,
		this);//Node manager for this variable (pDataVariableTag)
		pDataItem->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);			
	pDataItem->setValueHandling(UaVariable_Value_CacheIsSource);	
	addStatus = addNodeAndReference(pTagModbusType, pDataItem, OpcUaId_HasComponent);
	UA_ASSERT(addStatus.isGood());

	return ret;
}
UaStatusCode NmBuilding::getModbusConfig(OpcUa_UInt32 index, UaString & sName, OpcUa_UInt32 & address)
{
	address = index + 1;
	char name[100];
	sprintf(name,"TagOption_%d",address);
	sName = name;

	return OpcUa_Good;
}


