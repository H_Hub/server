#pragma once

#include "modbusobject.h"	
#include "modbusreadcoil.h"
#include "nmbuilding.h"
#include <userdatabase.h>
#include <opcua_dataitemtype.h>
#include <methodhandleuanode.h>
#include <methodmanager.h>
class NmBuilding;
class UaMethodGeneric;


class TagModbusObject :public ModbusObject, public MethodManager
{
	UA_DISABLE_COPY(TagModbusObject);
public :
	TagModbusObject(const UaString& name, const UaNodeId& newNodeId, const UaString& defaultLocaleId, NmBuilding* pNodeManager,
		OpcUa_UInt32 deviceAddress);
	virtual ~TagModbusObject(void) ;

	virtual UaStatus beginCall(MethodManagerCallback* pCallback, const ServiceContext& serviceContext, OpcUa_UInt32 callbackHandle, MethodHandle* pMethodHandle, const UaVariantArray& inputArguments) override ;
	///virtual UaNodeId typeDefinitionId() const;
	
	//override ModbusObject::call
	virtual UaStatus call(
		UaMethod* pMethod,
		const UaVariantArray& inputArgument,
		UaVariantArray& /*output Argument*/,
		UaStatusCodeArray& inputArgumentResults,
		UaDiagnosticInfos& /*inputArgumentDiag*/
		//,NmBuilding* pNodeManger
		) override;
	MethodManager* getMethodManager(UaMethod* pMethod) const override;

	OpcUa::DataItemType* pDataItem = NULL;

private:
	UaMethodGeneric* m_pMethodAdd;
	UaMethodGeneric* m_pMethodDel;
	UaMethodGeneric* m_pMethodAddReg;
	UaMethodGeneric* m_pMethodDelReg;
	NmBuilding* m_pNodeManager;

};
