#include "tagmongodb.h"
#include "tagmodbusobject.h"
#include <nodemanagerbase.h>

TagModbusObject::TagModbusObject(const UaString & name, const UaNodeId & newNodeId, const UaString & defaultLocaleId, NmBuilding * pNodeManager, OpcUa_UInt32 deviceAddress) : ModbusObject(name, newNodeId, defaultLocaleId, pNodeManager, deviceAddress), m_pNodeManager(pNodeManager)
{
	UaVariable* pInstanceDeclaration = NULL;
	
	UaStatus addStatus;

	BaUserData* pUserData = NULL;
	
	BaUserData* pUserDataStatus = NULL;
	

	OpcUa_Int16 nsIdx = pNodeManager->getNameSpaceIndex();
	UaPropertyMethodArgument* pPropertyArg = NULL;
	UaUInt32Array nullarray;
	
	
	
	//add method "Add_Node"
	UaString sName = "Add_Tag";
	UaString sNodeId = UaString("%1.%2").arg(newNodeId.toString()).arg(sName);
	m_pMethodAdd = new UaMethodGeneric(
		sName,
		UaNodeId(sNodeId, pNodeManager->getNameSpaceIndex()),
		 m_defaultLocaleId); 
	addStatus = pNodeManager->addNodeAndReference(this, m_pMethodAdd, OpcUaId_HasComponent);
	UA_ASSERT(addStatus.isGood());
	//add protperty "input arg"
	sName = "AddNodeSetValue";
	sNodeId = UaString("%1.%2").arg(m_pMethodAdd->nodeId().toString()).arg(sName);	
	pPropertyArg = new UaPropertyMethodArgument(
		UaNodeId(sNodeId, nsIdx),
		Ua_AccessLevel_CurrentRead,
		1, //number of argument
		UaPropertyMethodArgument::INARGUMENTS
	);
	pPropertyArg->setArgument(
		0, //Index of argument
		"Coil to Read",  //displayname of argument
		UaNodeId(OpcUaId_Double), // datatype of argument
		OpcUa_ValueRanks_Scalar,
		nullarray,
		UaLocalizedText("en","Input which coil from modbus slave"));
	addStatus = pNodeManager->addNodeAndReference(m_pMethodAdd, pPropertyArg, OpcUaId_HasProperty);
	UA_ASSERT(addStatus.isGood());

	
	//add method "Delete_Node"
	UaString sNameDel = "Del_Tag";
	UaString sNodeIdDel = UaString("%1.%2").arg(newNodeId.toString()).arg(sNameDel);
	m_pMethodDel = new UaMethodGeneric(
		sNameDel,
		UaNodeId(sNodeIdDel, pNodeManager->getNameSpaceIndex()),
		m_defaultLocaleId);
	addStatus = pNodeManager->addNodeAndReference(this, m_pMethodDel, OpcUaId_HasComponent);
	UA_ASSERT(addStatus.isGood());
		//add property input Arg
	sNameDel = "DelNameSetCoil";
	sNodeIdDel = UaString("%1.%2").arg(m_pMethodDel->nodeId().toString()).arg(sNameDel);
	pPropertyArg = new UaPropertyMethodArgument(
		UaNodeId(sNameDel, nsIdx),
		Ua_AccessLevel_CurrentRead,
		1, // number of argument
		UaPropertyMethodArgument::INARGUMENTS);
	pPropertyArg->setArgument(
		0,
		"Coil to Delete(DB)",
		UaNodeId(OpcUaId_Double),
		-1, //OpcUa_ValueRanks_Scalar(-1)
		nullarray,
		UaLocalizedText("en", "Specify Coil to Delete "));
	addStatus = pNodeManager->addNodeAndReference(m_pMethodDel, pPropertyArg, OpcUaId_HasProperty);
	UA_ASSERT(addStatus.isGood());

	
	//add method "Add_Reg"
	UaString sNameReg = "Add_Reg";
	UaString sNodeIdReg = UaString("%1.%2").arg(newNodeId.toString()).arg(sNameReg);
	m_pMethodAddReg = new UaMethodGeneric(
		sNameReg,
		UaNodeId(sNodeIdReg, pNodeManager->getNameSpaceIndex()),
		m_defaultLocaleId);
	addStatus = pNodeManager->addNodeAndReference(this, m_pMethodAddReg, OpcUaId_HasComponent);
	UA_ASSERT(addStatus.isGood());
	//add protperty "input arg"
	sNameReg = "AddRegSetValue";
	sNodeIdReg = UaString("%1.%2").arg(m_pMethodAddReg->nodeId().toString()).arg(sNameReg);
	pPropertyArg = new UaPropertyMethodArgument(
		UaNodeId(sNodeIdReg, nsIdx),
		Ua_AccessLevel_CurrentRead,
		1, //number of argument
		UaPropertyMethodArgument::INARGUMENTS
	);
	pPropertyArg->setArgument(
		0, //Index of argument
		"Register to Read",  //displayname of argument
		UaNodeId(OpcUaId_Double), // datatype of argument
		OpcUa_ValueRanks_Scalar, 
		nullarray,
		UaLocalizedText("en", "Input which register from modbus slave Port503"));
	addStatus = pNodeManager->addNodeAndReference(m_pMethodAddReg, pPropertyArg, OpcUaId_HasProperty);
	UA_ASSERT(addStatus.isGood());

	//ADD METHOD "Del_Reg"
	UaString sNameDelReg = "Del_Reg";
	UaString sNodeIdDelReg = UaString("%1.%2").arg(newNodeId.toString()).arg(sNameDelReg);
	m_pMethodDelReg = new UaMethodGeneric(
		sNameDelReg,
		UaNodeId(sNodeIdDelReg, pNodeManager->getNameSpaceIndex()),
		m_defaultLocaleId);
	addStatus = pNodeManager->addNodeAndReference(this, m_pMethodDelReg, OpcUaId_HasComponent);
	UA_ASSERT(addStatus.isGood());
	//add property input Arg
	sNameDelReg = "DelNameSetReg";
	sNodeIdDelReg = UaString("%1.%2").arg(m_pMethodDelReg->nodeId().toString()).arg(sNameDelReg);
	pPropertyArg = new UaPropertyMethodArgument(
		UaNodeId(sNameDelReg, nsIdx),
		Ua_AccessLevel_CurrentRead,
		1, // number of argument
		UaPropertyMethodArgument::INARGUMENTS);
	pPropertyArg->setArgument(
		0,
		"Register to Delete(DB)",
		UaNodeId(OpcUaId_Double),
		-1, //OpcUa_ValueRanks_Scalar(-1)
		nullarray,
		UaLocalizedText("en", "Specify Register to Delete "));
	addStatus = pNodeManager->addNodeAndReference(m_pMethodDelReg, pPropertyArg, OpcUaId_HasProperty);
	UA_ASSERT(addStatus.isGood());
}

TagModbusObject::~TagModbusObject(void)
{
}
UaStatus TagModbusObject::beginCall(MethodManagerCallback * pCallback, const ServiceContext & serviceContext, OpcUa_UInt32 callbackHandle, MethodHandle * pMethodHandle, const UaVariantArray & inputArguments)
{
	UaStatus ret;
	OpcUa_ReferenceParameter(serviceContext);
	UaVariantArray outputArguments;
	UaStatusCodeArray inputArgumentResults;
	UaDiagnosticInfos inputArgumentDiag;
	MethodHandleUaNode* pMethodHandleUaNode = static_cast<MethodHandleUaNode*> (pMethodHandle);
	UaMethod* pMethod = NULL;

	if (pMethodHandleUaNode)
	{
		pMethod = pMethodHandleUaNode->pUaMethod();
		if (pMethod)
		{
			ret = call(pMethod, inputArguments, outputArguments, inputArgumentResults, inputArgumentDiag);
			
		}
		else
		{
			assert(false);
			ret = OpcUa_BadInvalidArgument;
		}
		pCallback->finishCall(
			callbackHandle,
			inputArgumentResults,
			inputArgumentDiag,
			outputArguments, ret);
		ret = OpcUa_Good;
	}
	
	return ret;
}
UaStatus TagModbusObject::call(UaMethod * pMethod, const UaVariantArray & inputArgument, UaVariantArray &, UaStatusCodeArray & inputArgumentResults, UaDiagnosticInfos &)//, NmBuilding* pNodeManager)
{
	//auto nodeId = UaNodeId("coil1", 2);
	//auto p = m_pNodeManager->getNode(nodeId);
	UaStatus ret;
			///check if we have add method
		if (pMethod->nodeId() == m_pMethodAdd->nodeId())
		{
			//Number of input Argumnet mustbe 1
			if (inputArgument.length() != 1)
			{
				ret = OpcUa_BadInvalidArgument;
			}
			else
			{
				inputArgumentResults.create(1); // Tao mang ket qua co so phan tu tuong ung voi so  bien nhap vao
				///validate (xac nhan) input parameter
				if (inputArgument[0].Datatype != OpcUaType_Double)
				{
					ret = OpcUa_BadInvalidArgument;
					inputArgumentResults[0] = OpcUa_BadTypeMismatch;
				}
				else
				{
					OpcUa_Int32 i;
					UaVariant vTemp;
					OpcUa_Double val;
					
					vTemp = inputArgument[0];
					vTemp.toInt32(i);
					mongocxx::uri uri("mongodb://127.0.0.1:27017"); 
					mongocxx::client client(uri);
					mongocxx::database db = client["tagModbusDB"];
					mongocxx::collection col = db["ModbusCoil"];
					auto builder = bsoncxx::builder::stream::document{};
					bsoncxx::document::value doc_value = builder 
						<< "coil" 
						<< i
						<< bsoncxx::builder::stream::finalize;
					bsoncxx::document::view view = doc_value.view();
					col.insert_one(doc_value.view());				
					
					OpcUa::BaseDataVariableType* pDataVariable;
					
					UaVariant defaultValue;
					UaStatus addStatus;
					//Add Variable "AddedNode" as BaseDataVariable
					defaultValue.setString(m_pNodeManager->pModbusMaster->strVector[i].c_str());
					UaString sDisplayName = UaString("TagModbus_Coil%1").arg(i);
					UaString sNodeId = UaString("coil%1").arg(i);
					pDataVariable = new OpcUa::BaseDataVariableType(
						UaNodeId(sNodeId,m_pNodeManager->getNameSpaceIndex()),
						sDisplayName,
						m_pNodeManager->getNameSpaceIndex(),
						defaultValue,
						Ua_AccessLevel_CurrentRead | Ua_AccessLevel_CurrentWrite,
						m_pNodeManager);//Node manager for this variable (pDataVariableTag)
					pDataVariable->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
					pDataVariable->setValueHandling(UaVariable_Value_CacheIsSource);
					addStatus = m_pNodeManager->addNodeAndReference(m_pNodeManager->pFolder, pDataVariable, OpcUaId_HasComponent);
					UA_ASSERT(addStatus.isGood());	
					m_pNodeManager->coil.push_back(i); // update vector coil when add_method happen
				
				}
			}
		}		
		if (pMethod->nodeId() == m_pMethodDel->nodeId())
		{
			if (inputArgument.length() != 1)
			{
				return OpcUa_BadInvalidArgument;
			}
			else
			{
				inputArgumentResults.create(1);
				if (inputArgument[0].Datatype != OpcUaId_Double)
				{
					ret = OpcUa_BadInvalidArgument;
					inputArgumentResults[0] = OpcUa_BadTypeMismatch;
				}
				else
				{							
					OpcUa_Int32 i;
					UaVariant vTemp;
					OpcUa_Double val;
					vTemp = inputArgument[0]; 
					vTemp.toInt32(i);
					
					//Del vector<> coil_____update vector coil, khi xoa 1 tag thi vector coil can dc update so phan tu
					for (int j=0; j< m_pNodeManager->coil.size(); j++)			
					{
						if (m_pNodeManager->coil[j] == i) // i vai tro la gia tri trong vector, = inputArgument[0]
						{
							m_pNodeManager->coil.erase(m_pNodeManager->coil.begin() + j);
						}
					}		
					//Del MongoDB
					mongocxx::uri uri("mongodb://127.0.0.1:27017");
					mongocxx::client client(uri);
					mongocxx::database db = client["tagModbusDB"];
					mongocxx::collection col = db["ModbusCoil"];
					auto builder = bsoncxx::builder::stream::document{};

					col.delete_one(builder << "coil" << i << finalize);
										
					//Del Node					
					OpcUa_Int16 nsIdx = m_pNodeManager ->getNameSpaceIndex();
					UaString sNodeId = UaString("coil%1").arg(i); 
					std::string a = sNodeId.toUtf8();
					///UaNodeId folderId("ModbusOpcUa", nsIdx);
					UaNodeId nodeId(sNodeId, m_pNodeManager->getNameSpaceIndex());
					UaNode* pNode = m_pNodeManager->getNode(nodeId);		
					if (pNode)
					{
						m_pNodeManager->deleteUaNode(pNode, OpcUa_True, OpcUa_True, OpcUa_True); 					
						pNode->releaseReference();
						pNode = NULL; 
						
					}
					else
					{
					}
				}
			}
		}
		if (pMethod->nodeId() == m_pMethodAddReg->nodeId()) //form from: m_pMehtodAdd
		{
			if (inputArgument.length() != 1)
			{
				ret = OpcUa_BadInvalidArgument;
			}
			else
			{
				inputArgumentResults.create(1);
				if (inputArgument[0].Datatype != OpcUaType_Double)
				{
					ret = OpcUa_BadInvalidArgument;
					inputArgumentResults[0] = OpcUa_BadTypeMismatch;
				}
				else
				{
					UaVariant vTemp(inputArgument[0]);
					OpcUa_Int32 i;
					vTemp.toInt32(i);
					///update for mongodb
					mongocxx::uri uri("mongodb://127.0.0.1:27017");
					mongocxx::client client(uri);
					mongocxx::database db = client["tagModbusDB"];
					mongocxx::collection col = db["ModbusRegister"];
					auto builder = bsoncxx::builder::stream::document{}; 
					bsoncxx::document::value doc_value = builder << "Register" << i << bsoncxx::builder::stream::finalize;
					bsoncxx::document::view view = doc_value.view();
					col.insert_one(view);
					///add tag
					OpcUa::BaseDataVariableType* pDataVariable;
					UaVariant value;
					value.setInt32(m_pNodeManager->pHoldingRegister->repVector[i]);
					UaStatus addStatus;

					UaString sDisplayName = UaString("TagModbus_Register%1").arg(i);
					UaString sNodeId = UaString("reg%1").arg(i);
					pDataVariable = new OpcUa::BaseDataVariableType(
						UaNodeId(sNodeId, m_pNodeManager->getNameSpaceIndex()),
						sDisplayName,
						m_pNodeManager->getNameSpaceIndex(),
						value,
						Ua_AccessLevel_CurrentRead | Ua_AccessLevel_CurrentWrite,
						m_pNodeManager);//Node manager for this variable (pDataVariableTag)
					pDataVariable->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
					pDataVariable->setValueHandling(UaVariable_Value_CacheIsSource);
					addStatus = m_pNodeManager->addNodeAndReference(m_pNodeManager->pFolder, pDataVariable, OpcUaId_HasComponent);
					UA_ASSERT(addStatus.isGood());

					m_pNodeManager->reg.push_back(i);
				}
			}

		}
		if (pMethod->nodeId() == m_pMethodDelReg->nodeId())
		{
			if (inputArgument.length() != 1)
			{
				ret = OpcUa_BadInvalidArgument;
			}
			else
			{
				inputArgumentResults.create(1);
				if (inputArgument[0].Datatype != OpcUaId_Double)
				{
					ret = OpcUa_BadInvalidArgument;
					inputArgumentResults[0] = OpcUa_BadTypeMismatch;
				}
				else
				{
					OpcUa_Int32 i;
					UaVariant tempValue;
					tempValue = inputArgument[0];
					tempValue.toInt32(i);

					///Del vector<> reg
					for (int j = 0; j < m_pNodeManager->reg.size(); j++)
					{
						if (m_pNodeManager->reg[j] == i)
						{
							m_pNodeManager->reg.erase(m_pNodeManager->reg.begin() + j);
						}
					}
					///Del MongoDB
					mongocxx::uri uri("mongodb://127.0.0.1:27017");
					mongocxx::client client(uri);
					mongocxx::database db = client["tagModbusDB"];
					mongocxx::collection col = db["ModbusRegister"];
					auto builder = bsoncxx::builder::stream::document{};
					col.delete_one(builder << "Register" << i << finalize);
					///Del Node
					OpcUa_Int16 nsIdx = m_pNodeManager->getNameSpaceIndex();
					UaString sNodeId = UaString("reg%1").arg(i);
					std::string a = sNodeId.toUtf8();
					UaNodeId nodeId(sNodeId, m_pNodeManager->getNameSpaceIndex());
					UaNode* pNode = m_pNodeManager->getNode(nodeId);
					if (pNode)
					{
						m_pNodeManager->deleteUaNode(pNode, OpcUa_True, OpcUa_True, OpcUa_True);
						pNode->releaseReference();
						pNode = NULL;
					}
				}
			}
		}
	
		return ret;
}

MethodManager * TagModbusObject::getMethodManager(UaMethod * pMethod) const
{
	OpcUa_ReferenceParameter(pMethod);
	return (MethodManager*)this;
}



