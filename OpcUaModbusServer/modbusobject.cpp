#include "modbusobject.h"
#include <opcua_dataitemtype.h>
#include <methodhandleuanode.h>

ModbusObject::ModbusObject(const UaString & name, const UaNodeId & newNodeId, const UaString & defaultLocleId, NmBuilding* pNodeManager,
	OpcUa_UInt32 deviceAddress)
	:UaObjectBase(name, newNodeId, defaultLocleId), 
	m_pSharedMutex(NULL),
	m_deviceAddress(deviceAddress),
	//m_pNodeManager(pNodeManager), 
	m_pMethodAdd(NULL) ////______________________________________

{
	
	m_pSharedMutex = new UaMutexRefCounted;
	OpcUa::BaseDataVariableType* pDataVariable= NULL;
	UaVariable* pInstanceDeclaration = NULL;
	//OpcUa::DataItemType* pDataItem;
	UaStatus addStatus;
	

	BaUserData* pUserData = NULL;
	BaUserData* pUserDataStatus = NULL;

	//Method helper
	OpcUa_Int16 nsIdx = pNodeManager->getNameSpaceIndex();
	UaPropertyMethodArgument* pPropertyArg = NULL;
	UaUInt32Array nullarray;
	

	//Add variable "Point" object
	pInstanceDeclaration = pNodeManager->getInstanceDeclarationVariable("point");
	UA_ASSERT(pInstanceDeclaration != NULL);
	pDataItem = new OpcUa::DataItemType(
		this,
		pInstanceDeclaration,
		pNodeManager,
		m_pSharedMutex);
	addStatus = pNodeManager->addNodeAndReference(this, pDataItem, OpcUaId_HasComponent);
	UA_ASSERT(addStatus.isGood());

	
	//Store information needed to access device
	pUserData = new BaUserData(/*OpcUa_False*/);
	pDataItem->setUserData(pUserData);
	///change value handling to get read and write calls to the nodemanager
	pDataItem->setValueHandling(UaVariable_Value_CacheIsSource);


	//Add object "StatusCoil"
	pInstanceDeclaration = pNodeManager->getInstanceDeclarationVariable("statuscoil");
	UA_ASSERT(pInstanceDeclaration != NULL);
	pDataItem = new OpcUa::DataItemType(
		this,
		pInstanceDeclaration,
		pNodeManager,
		m_pSharedMutex);
	pDataItem->setValueHandling(UaVariable_Value_CacheIsSource);
	addStatus = pNodeManager->addNodeAndReference(this, pDataItem, OpcUaId_HasComponent);
	UA_ASSERT(addStatus.isGood());


	//Add object "Voltage"
	pInstanceDeclaration = pNodeManager->getInstanceDeclarationVariable("voltage");
	UA_ASSERT(pInstanceDeclaration != NULL);
	pDataItem = new OpcUa::DataItemType(
		this,
		pInstanceDeclaration,
		pNodeManager,
		m_pSharedMutex);
	pDataItem->setAccessLevel(Ua_AccessLevel_CurrentRead | Ua_AccessLevel_CurrentWrite);

	///Set value to variable by"setvalue"
	UaVariant v;
	UaDataValue dataValue;
	v.setDouble(15);
	dataValue.setValue(v, OpcUa_False, OpcUa_True);
	//UaDataValue dt(v, OpcUa_Good, UaDateTime::now(), UaDateTime::now());
	pDataItem->setValue(NULL, dataValue, OpcUa_False);
	addStatus = pNodeManager->addNodeAndReference(this, pDataItem, OpcUaId_HasComponent);
	UA_ASSERT(addStatus.isGood());
	   	
	}

ModbusObject::~ModbusObject(void)
{
	if (m_pSharedMutex)
	{
		// Release our local reference
	}m_pSharedMutex->releaseReference();
	m_pSharedMutex = NULL;
	
}

OpcUa_Byte ModbusObject::eventNotifier() const
{
	
	return Ua_EventNotifier_None; // returns the value of the EventNotifier attribute
}

