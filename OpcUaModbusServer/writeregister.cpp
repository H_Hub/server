#include "writeregister.h"


void runFC06(boost::asio::io_service* service)
{
	service->run();
}

WriteRegister::WriteRegister(): sock (service), work (service)
{
	threadWork = std::thread(std::bind(&runFC06, &service));
	
}

WriteRegister::~WriteRegister()
{
	sock.close();
	service.stop();
	threadWork.join();
}

void WriteRegister::connect(boost::asio::ip::tcp::endpoint & ep)
{
	memset(frame, 0, 12);

	frame[0] = 0;
	frame[1] = 1;
	frame[2] = 0;
	frame[3] = 0;
	frame[4] = 0;
	frame[5] = 6;
	frame[6] = 1;
	frame[7] = 6; //functioncode;
	frame[8] = (uint8_t)(addr >> 8);
	frame[9] = (uint8_t)(addr);
	frame[10] = (uint8_t)(num_val >> 8);
	frame[11] = (uint8_t)(num_val);
	sock.async_connect(ep, boost::bind(&WriteRegister::connectHandler, this, boost::asio::placeholders::error));
}

void WriteRegister::connectHandler(const boost::system::error_code & ec)
{

	if (!ec)
	{
		printf("FC06-CONNECTED TO SERVER\n");
		writeFrame();
	}
	else
	{
		printf("\nNo connection!\n");
	}
}

void WriteRegister::writeFrame()
{
	sock.async_write_some(boost::asio::buffer(frame, 12), boost::bind(&WriteRegister::writeFrameHanlder, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void WriteRegister::writeFrameHanlder(const boost::system::error_code & e, size_t byte)
{
	if (!e)
	{
		read();
	}
	else
	{
		sock.close();
	}
}

void WriteRegister::read()
{
	memset(respone, 0, 1024);
	sock.async_read_some(boost::asio::buffer(respone, 1024), boost::bind(&WriteRegister::readHandler, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void WriteRegister::readHandler(const boost::system::error_code & e, size_t byte)
{
	if (!e)
	{
		const int size = static_cast<int>(respone[8]);
		int rep[1024];// = new char[size];
		
		for (int i = 0, j = 9; i < size, j < size + 9; i++, j++)
		{
			rep[i] = respone[j];
		}
		int m = 0;
		while (m < size)
		{
			repVector.push_back(rep[m] * 256 + (rep[m + 1]));
			m = m + 2;
		}
		
	}
	else
	{
		std::cout << "ERROR";
	}
}
