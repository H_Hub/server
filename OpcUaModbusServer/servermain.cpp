
#include "tagmongodb.h"
#include "nmbuilding.h"
#include <opcserver.h>
#include <xmldocument.h>
#include <uaplatformdefs.h>
#include <uaplatformlayer.h>
#include <uathread.h>
#include <iostream>
#include <shutdown.h>

#include <uathread.h>
#include <conio.h>
#include <mutex>
#include <vector>


int OpcServerMain(const char* szAppPath)
{
	int ret = 0;
	UaXmlDocument::initParser();
	ret = UaPlatformLayer::init();
	if (ret == 0)
	{
		UaString sConfigFileName(szAppPath);
		sConfigFileName += "/ServerConfig.xml";
		OpcServer* pServer = new OpcServer;
		pServer->setServerConfig(sConfigFileName, szAppPath);	
		
		boost::asio::ip::tcp::endpoint ep(boost::asio::ip::address_v4::from_string("127.0.0.1"), 502);
		boost::asio::ip::tcp::endpoint ep503(boost::asio::ip::address_v4::from_string("127.0.0.1"), 503);
		
		///Tao 1 node model moi
		NmBuilding *pMyNodeManager = new NmBuilding();				

		pServer->addNodeManager(pMyNodeManager);	
		
		std::thread thr2([&pServer, &ret, &pMyNodeManager]() {
			ret = pServer->start();
			if (ret != 0)
			{
				delete pServer;
				pServer = 0;
			}
			if (ret == 0)
			{
			
				cout << "\n*************************************\n";
				printf("Press %s to shutdown server\n", SHUTDOWN_SEQUENCE);
				cout << "*************************************\n";
				
				while (ShutDownFlag() == 0)
				{					
					UaThread::msleep(1000); //circle 1000ms checking live or not
					
					pMyNodeManager->readValues();
					
				}
				cout << "*************************************\n";
				cout << "Shutting down Server\n";
				cout << "*************************************\n";
				

				//-Stop OPC server--------------
				//Stop server and  wait 2 seconds if clients are connected 
				//to allow them to disconnect after  they received the shutdown signal
				pServer->stop(2, UaLocalizedText("", "User shutdown"));
				delete pServer;
				pServer = NULL;
				
			}
		}
		);
		//ret = pServer->start();
		pMyNodeManager->pModbusMaster->connect(ep);
		pMyNodeManager->pHoldingRegister->connect(ep503);
		///Read Modbus
		
	
		thr2.join();
	
		
		
	}
	UaPlatformLayer::cleanup();
	UaXmlDocument::cleanupParser();
	return ret;
}
int main()
{	
	mongocxx::instance instance{};
	int ret = 0;
	RegisterSignalHandler();
	char* pszAppPath = getAppPath(); //extract application path

	
	ret = OpcServerMain(pszAppPath);
	if (pszAppPath) delete[] pszAppPath;
	
	_getch();
	return ret;
}