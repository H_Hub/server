#include "modbuswritecoil.h"

void run2(boost::asio::io_service* service)
{
	service->run();
}

ModbusWriteCoil::ModbusWriteCoil():sock(service), work(service) 
{
	threadWork = std::thread(std::bind(&run2, &service));
		
}

ModbusWriteCoil::~ModbusWriteCoil()
{
	sock.close();
	service.stop();
	threadWork.join();
}

void ModbusWriteCoil::connect(boost::asio::ip::tcp::endpoint & ep)
{
	memset(frameSender, 0, 12);
	//writtenAddr_ = 3;
	//valueWrite_ = 255; //true (1) value
	frameSender[0] = 0;
	frameSender[1] = 1;
	frameSender[2] = 0;
	frameSender[3] = 0;
	frameSender[4] = 0;
	frameSender[5] = 6;
	frameSender[6] = 1;
	frameSender[7] = 5;// FUNCTIONCODE 05
	frameSender[8] = (uint8_t)(writtenAddr_ >> 8);
	frameSender[9] = (uint8_t)(writtenAddr_);
	frameSender[10] = (uint8_t)(valueWrite_);
	frameSender[11] = (uint8_t)(valueWrite_ >> 8);

	sock.async_connect(ep, boost::bind(&ModbusWriteCoil::connectHandler, this, boost::asio::placeholders::error));
}

void ModbusWriteCoil::connectHandler(const boost::system::error_code & ec)
{
	if (!ec)
	{
		std::cout << "CONNECTED TO MODBUS SLAVE, FC05" << std::endl;
		writeFrame();
	}
	else
	{
		std::cout << "No connection (FC05)!" << std::endl;
	}
}

void ModbusWriteCoil::writeFrame()
{
	sock.async_write_some(boost::asio::buffer(frameSender, 12), boost::bind(&ModbusWriteCoil::writeFrameHandler, this,boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void ModbusWriteCoil::writeFrameHandler(const boost::system::error_code & ec, size_t byte)
{
	if (!ec)
	{
		readCoil();
	}
	else
	{
		sock.close();
	}

}

void ModbusWriteCoil::readCoil()
{
	memset(response, 0, 1024);
	sock.async_read_some(boost::asio::buffer(response, 1024), boost::bind(&ModbusWriteCoil::readCoilHandler,this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void ModbusWriteCoil::readCoilHandler(const boost::system::error_code & ec, size_t byte)
{
	if (!ec)
	{
		std::string data_rep;
		if (response[10] == 255)
		{
			data_rep = "Write to coil : True";
			std::cout << data_rep << std::endl;
		}
		if (response[10] == 0)
		{
			data_rep = "Write to coil : False";
			std::cout << data_rep << std::endl;
		}
		
	}
	else
	{
		std::cout << "Reading error!" << std::endl;
	}
}
