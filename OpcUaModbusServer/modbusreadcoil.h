#pragma once

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <cstdint>
#include <thread>
#include <string>
#include <cstdio>
#include <iostream>

class ModbusReadCoil
{
public:
	ModbusReadCoil();
	virtual ~ModbusReadCoil();
	void connect(boost::asio::ip::tcp::endpoint& ep);
	void connectHandler(const boost::system::error_code& ec);
	void writeFrame();
	void writeFrameHandler(const boost::system::error_code& ec, size_t byte);
	void readCoil();
	void readCoilHandler(const boost::system::error_code& ec, size_t byte);

	void timeout(const boost::system::error_code&e);

	std::string toBool(char* rep);
	uint32_t toInt(uint64_t data);

	std::vector<std::string> strVector;
	std::vector<std::string> tempVector;

private:
	std::thread threadWork;
	boost::asio::io_service service;
	boost::asio::io_service::work work;
	boost::asio::ip::tcp::socket sock;
	uint8_t response[1024];
	uint8_t frameSender[12];
	short startAddr_, numberOfCoil_;
	boost::asio::deadline_timer t;
	
	void wait();
};
