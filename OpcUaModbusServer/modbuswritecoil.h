#pragma once

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <cstdint>
#include <thread>
#include <string>
#include <cstdio>
#include <iostream>

class ModbusWriteCoil
{
public:
	ModbusWriteCoil();
	virtual ~ModbusWriteCoil();
	void connect(boost::asio::ip::tcp::endpoint& ep);
	void connectHandler(const boost::system::error_code& ec);
	void writeFrame();
	void writeFrameHandler(const boost::system::error_code& ec, size_t byte);
	void readCoil();
	void readCoilHandler(const boost::system::error_code& ec, size_t byte);
	
	int valueWrite_;
	short writtenAddr_;
private:
	std::thread threadWork;
	boost::asio::io_service service;
	boost::asio::io_service::work work;
	boost::asio::ip::tcp::socket sock;
	uint8_t response[1024];
	uint8_t frameSender[12];
	
};
