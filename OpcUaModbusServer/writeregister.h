#pragma once
#include "holdingregister.h"

class WriteRegister
{
public:
	WriteRegister();
	virtual ~WriteRegister();
	void connect(boost::asio::ip::tcp::endpoint & ep);
	void connectHandler(const boost::system::error_code & ec);
	void writeFrame();
	void writeFrameHanlder(const boost::system::error_code& e, size_t byte);
	void read();
	void readHandler(const boost::system::error_code& e, size_t byte);
	std::vector<int> repVector;
	int addr , num_val;
private:
	uint8_t respone[1024];
	std::thread threadWork;
	boost::asio::io_service service;
	boost::asio::io_service::work work;
	boost::asio::ip::tcp::socket sock;
	uint8_t frame[12];
};