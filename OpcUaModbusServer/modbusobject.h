#pragma once
#include <uaobjecttypes.h>
#include <nodemanagerbase.h>
#include <userdatabase.h>
#include "nmbuilding.h"
#include <methodmanager.h>
#include <opcua_dataitemtype.h>


class NmBuilding;
class UaMethodGeneric;//<-- method support


class ModbusObject : public UaObjectBase
{
	UA_DISABLE_COPY(ModbusObject);

public:
	ModbusObject(const UaString& name, const UaNodeId& newNodeId, const UaString& defaultLocleId, NmBuilding* pNodeManager, OpcUa_UInt32 deviceAddress);
	virtual ~ModbusObject(void);
	OpcUa::DataItemType* pDataItem = NULL;
	OpcUa_Byte eventNotifier() const;

	
	virtual UaStatus call(UaMethod* /*pMethod*/, 
		const UaVariantArray&/*inputArgument*/,
		UaVariantArray&	,
		UaStatusCodeArray&/*inputArgumentResults*/,
		UaDiagnosticInfos&//,
		//NmBuilding * pNodeManager
		)
	{
		return OpcUa_BadMethodInvalid;
	}


protected:
	UaMutexRefCounted* m_pSharedMutex;
	OpcUa_UInt32 m_deviceAddress;

private:
	
	UaMethodGeneric* m_pMethodAdd;

};

class BaUserData : public UserDataBase
{
	UA_DISABLE_COPY(BaUserData);
public:
	BaUserData()
	{
	}
	virtual ~BaUserData() {}

};
