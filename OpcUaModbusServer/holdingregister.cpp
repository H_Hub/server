#include "holdingregister.h"


void runFC03(boost::asio::io_service* service)
{
	service->run();
}

HoldingRegister::HoldingRegister() : sock(service), work(service), t(service)
{
	threadWork = std::thread(std::bind(&runFC03, &service));
	memset(frame, 0, 12);
	int addr=0, num_val=6;
	frame[0] = 0;
	frame[1] = 3;
	frame[2] = 0;
	frame[3] = 0;
	frame[4] = 0;
	frame[5] = 6;
	frame[6] = 1;
	frame[7] = 3; //functioncode;
	frame[8] = (uint8_t)(addr >> 8);
	frame[9] = (uint8_t)(addr);
	frame[10] = (uint8_t)(num_val >> 8);
	frame[11] = (uint8_t)(num_val);
}

HoldingRegister::~HoldingRegister()
{
	sock.close();
	service.stop();
	threadWork.join();
}

void HoldingRegister::connect(boost::asio::ip::tcp::endpoint & ep)
{
	sock.async_connect(ep, boost::bind(&HoldingRegister::connectHandler, this, boost::asio::placeholders::error));
}

void HoldingRegister::connectHandler(const boost::system::error_code & ec)
{
	if (!ec)
	{
		printf("FC03-CONNECTED TO SERVER\n");
		writeFrame();
	}
	else
	{
		printf("\nNo connection!\n");
	}
}

void HoldingRegister::writeFrame()
{
	sock.async_write_some(boost::asio::buffer(frame, 12), boost::bind(&HoldingRegister::writeFrameHanlder, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void HoldingRegister::writeFrameHanlder(const boost::system::error_code & e, size_t byte)
{
	if (!e)
	{
		read();
	}
	else
	{
		sock.close();
	}
}

void HoldingRegister::read()
{
	memset(respone, 0, 1024);
	tempVector = repVector;
	repVector.clear();
	sock.async_read_some(boost::asio::buffer(respone, 1024), boost::bind(&HoldingRegister::readHandler, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
	wait();
}

void HoldingRegister::readHandler(const boost::system::error_code & e, size_t byte)
{
	if (!e)
	{
		const int size = static_cast<int>(respone[8]);
		int rep[1024];// = new char[size];
		for (int i = 0, j = 9; i < size, j < size + 9; i++, j++)
		{
			rep[i] = respone[j];
		}
		int m = 0;
		while (m < size)
		{
			repVector.push_back(rep[m] * 256 + (rep[m + 1]));
			m = m + 2;
		}
		if (tempVector == repVector)
		{
			/*for (auto k : tempVector)
			{
				std::cout << k << " ";
			}
			std::cout << "\n";*/
		}
		if (tempVector != repVector)
		{
			for (auto k : repVector)
			{
				std::cout << k << " ";
			}
			std::cout << "\n";
		}
		
	}

	else
	{
		std::cout << "Reading error!\n";
	}

}

void HoldingRegister::timeout(const boost::system::error_code & e)
{
	if (e)
		return;
	writeFrame();
}

void HoldingRegister::wait()
{
	t.expires_from_now(boost::posix_time::seconds(5));
	t.async_wait(boost::bind(&HoldingRegister::timeout, this, boost::asio::placeholders::error));
}
