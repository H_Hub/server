#ifndef _NMBUILDING_H_
#define _NMBUILDING_H_

#include <nodemanagerbase.h>
#include <opcua_dataitemtype.h>
#include "modbusreadcoil.h"
#include "modbuswritecoil.h"
#include "holdingregister.h"
#include "writeregister.h"

class ModbusReadCoil;
class UaMethodGeneric;

class NmBuilding : public NodeManagerBase
{
	UA_DISABLE_COPY(NmBuilding);
public:
	NmBuilding();
	virtual ~NmBuilding();
	virtual UaStatus afterStartUp() override;
	virtual  UaStatus beforeShutDown() override;
	
	//IOManagerUaNode implementation, update Data
	virtual UaStatus readValues(/*const UaVariableArray& arrUaVariables, UaDataValueArray& arrUaDataValues*/);	

	virtual OpcUa_Boolean beforeSetAttributeValue(Session* pSession, UaNode* pNode, OpcUa_Int32 attributeId, const UaDataValue& dataValue, OpcUa_Boolean& checkWriteMask) override;
	//virtual void afterGetAttributeValue(Session * pSession, UaNode* pNode, OpcUa_Int32 attributeId,  UaDataValue& dataValue) override;

	UaVariable* getInstanceDeclarationVariable(UaString stringIdentifier);
	ModbusReadCoil* pModbusMaster = new ModbusReadCoil();
	ModbusWriteCoil* pModbusWriteCoil = new ModbusWriteCoil();
	HoldingRegister* pHoldingRegister = new HoldingRegister();
	WriteRegister* pWriteRegister = new WriteRegister();
	OpcUa_Int32 num;

	//SUPPORT FOE METHOD**************
		UaFolder* pFolder = NULL;
	vector<int32_t> coil; //store value query from mongodb
	vector<int32_t> reg;
private:
	UaStatus createTypeNode();
	UaStatusCode getModbusConfig(OpcUa_UInt32 index, UaString& sName, OpcUa_UInt32& address);	
	//Method helpers
	OpcUa::BaseMethod* pMethod = NULL;
	UaPropertyMethodArgument* pPropertyArg = NULL;
	UaUInt32Array nullarray;
	UaVariableArray  arrUaVariables;
	UaDataValueArray  arrUaDataValues;
	UaMethodGeneric* m_pMethod;
		
};
#endif